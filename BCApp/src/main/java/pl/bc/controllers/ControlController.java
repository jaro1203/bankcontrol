package pl.bc.controllers;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import pl.bc.entity.Branch;
import pl.bc.entity.Commission;
import pl.bc.entity.Employee;
import pl.bc.entity.Result;
import pl.bc.entity.Test;
import pl.bc.entity.TestResult;
import pl.bc.entity.Type;
import pl.bc.lists.ResultList;
import pl.bc.services.BranchService;
import pl.bc.services.CommissionService;
import pl.bc.services.EmployeeService;
import pl.bc.services.ResultService;
import pl.bc.services.TestResultService;
import pl.bc.services.TestService;
import pl.bc.services.TypeService;

@Controller
public class ControlController {

	@Autowired
	CommissionService commissionService;
	@Autowired
	TypeService typeService;
	@Autowired
	BranchService branchService;
	@Autowired
	EmployeeService employeeService;
	@Autowired
	ResultService resultService;
	@Autowired
	TestService testService;
	@Autowired
	TestResultService testResultService;

	//User-Kontroler
	
	@RequestMapping("/commissions")
	public String allCommissions(Model model) {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String loggedEmployee = authentication.getName();
		Employee employee = new Employee();
		employee = employeeService.getEmployee(Long.parseLong(loggedEmployee));
		List<Commission> commissions = commissionService.getCommissionsByEpn(employee);
		model.addAttribute("commissions", commissions);
		return "user/allCommissions";
	}
	  
	@RequestMapping("/commissions/{id}")
	public String viewCommission(Model model, @PathVariable("id") Long id) {
		Commission commission = commissionService.getCommissionById(id).orElse(null);
		model.addAttribute("commission", commission);
		List<Result> result = resultService.getResultByCommission(id);
		model.addAttribute("result", result);
		List<Test> tests = testService.getTestsByCommissionId(id);
		model.addAttribute("tests", tests);
	  return "user/viewCommission"; 
	  }
	  
	@RequestMapping(value = "/commissions/control/{id}")
	public String controlForm(Model model, @PathVariable("id") Long id) {
		Commission commission = commissionService.getCommissionById(id).orElse(null);
		model.addAttribute("commission", commission);
		List<Result> results = resultService.getResultByCommission(id);
		model.addAttribute("results", results);
		List<Result> resultsList = new ArrayList<>();
		results.iterator().forEachRemaining(resultsList::add);
		model.addAttribute("resultsListForm", new ResultList(resultsList));
		List<Test> tests = testService.getAll();
		model.addAttribute("tests", tests);
		List<TestResult> testResult = testResultService.getAll();
		model.addAttribute("testResult", testResult);
		return "user/controlForm";
	}
	  
	@RequestMapping(value = "/control/{id}", method = { RequestMethod.POST })
	public String control(Model model, @PathVariable("id") Long id, @ModelAttribute ResultList resultsListForm,
			BindingResult bindingResult, Errors errors) {
		if (bindingResult.hasErrors()) {
			return "user/controlForm";
		} else {
			commissionService.saveControlResult(id,resultsListForm);
			return "redirect:/commissions";
		}
	}

	@RequestMapping(value = "/commissions/edit/{id}")
	public String editResult(Model model, @PathVariable("id") Long id) {
		Commission commission = commissionService.getCommissionById(id).orElse(null);
		model.addAttribute("commission", commission);
		List<Result> result = resultService.getResultByCommission(id);
		model.addAttribute("result", result);
		List<Test> tests = testService.getTestsByCommissionId(id);
		model.addAttribute("tests", tests);
		List<Result> resultsList = new ArrayList<>();
		result.iterator().forEachRemaining(resultsList::add);
		model.addAttribute("resultsListForm", new ResultList(resultsList));
		List<TestResult> testResult = testResultService.getAll();
		model.addAttribute("testResult", testResult);
		/*
		 * List<TestResult> wynikTestu = testResultService.getAll();
		 * model.addAttribute("wynikTestu", wynikTestu);
		 */
		return "user/editResult";
	}

	//Admin
	@RequestMapping("/admin/commissions")
	public String allCommissionsAdmin(HttpServletRequest request, Model model) {
		int page = 0;
		int size = 5;
		if (request.getParameter("page") != null && !request.getParameter("page").isEmpty()) {
			page = Integer.parseInt(request.getParameter("page")) - 1;
		}
		if (request.getParameter("size") != null && !request.getParameter("size").isEmpty()) {
			size = Integer.parseInt(request.getParameter("size"));
		}
		Page<Commission> commissions=commissionService.getAll(page,size);
		model.addAttribute("commissions", commissions);
		return "admin/allCommissions";
	}
	
	@RequestMapping(value = {"/admin/addCommission" })
	public String addCommissionForm(Model model) {
		model.addAttribute("commission", new Commission());
		List<Type> types = typeService.getActiveTypes();
		model.addAttribute("types", types);
		List<Branch> branches = branchService.getActiveBranches();
		model.addAttribute("branches", branches);
		List<Employee> controlEmp = employeeService.getControlEmployees();
		model.addAttribute("controlEmp", controlEmp);
		return "admin/addCommission";
	}
	
	@RequestMapping(value = "/addedCommission", method = RequestMethod.POST)
	public String addCommission(Model model,@Valid Commission commission, Errors errors) {
		if (errors.hasErrors()) {
			model.addAttribute("commission", commission);
			return "admin/addCommission";
		}
		commissionService.saveCommission(commission);
		return "redirect:/admin/commissions";
	}
	
	@RequestMapping("admin/commissions/edit/{id}")
	public String editCommissionForm(Model model, @PathVariable("id") Long id) {
		Commission commission = commissionService.getCommissionById(id).orElse(null);
		model.addAttribute("commission", commission);
		List<Type> types = typeService.getActiveTypes();
		model.addAttribute("types", types);
		List<Branch> branches = branchService.getActiveBranches();
		model.addAttribute("branches", branches);
		List<Employee> controlEmp = employeeService.getControlEmployees();
		model.addAttribute("controlEmp", controlEmp);
		return "admin/editCommission";
	}

	@RequestMapping(value = "/admin/commissions/editCommission/{id}", method = RequestMethod.POST)
	public String editCommission(Model model, @Valid Commission commission,
			BindingResult bindingResult, Errors errors, @PathVariable("id") Long id) {
		if (bindingResult.hasErrors()) {
			model.addAttribute("commission", commission);
			return "admin/editCommission";
		} else {
			commissionService.updateCommission(commission);
			return "redirect:/admin/commissions";
		}
	}

	@RequestMapping("/admin/commissions/delete/{id}")
	public String deleteCommission(@PathVariable("id") Long id) {
		commissionService.deleteCommission(id);
		return "redirect:/admin/commissions";
	}

}
