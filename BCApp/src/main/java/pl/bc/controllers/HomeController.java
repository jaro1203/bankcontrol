package pl.bc.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;


@Controller
public class HomeController {
	@RequestMapping(value = { "/", "/login" }, method = RequestMethod.GET)
	public ModelAndView login() {
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("login");
		return modelAndView;
	}
	
	@RequestMapping("/admin/adminConsole")
	public String adminConsole(Model model) {
		return "admin/adminConsole";
	}

	@RequestMapping(value = "/home", method = RequestMethod.GET)
	public ModelAndView home() {
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("home");
		return modelAndView;
	}
	
	/*
	 * @RequestMapping(value = "/admin/home", method = RequestMethod.GET) public
	 * ModelAndView adminHome() { ModelAndView modelAndView = new ModelAndView();
	 * modelAndView.setViewName("/admin/adminHome"); return modelAndView; }
	 */

}