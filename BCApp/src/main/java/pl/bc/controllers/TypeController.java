package pl.bc.controllers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import pl.bc.entity.Type;
import pl.bc.entity.Test;
import pl.bc.entity.TestResult;
import pl.bc.enums.ControlStatus;
import pl.bc.enums.TestStatus;
import pl.bc.lists.TestsList;
import pl.bc.services.TypeService;
import pl.bc.services.TestService;
import pl.bc.services.TestResultService;

@Controller
public class TypeController {
	@Autowired
	TypeService typeService;
	@Autowired
	TestService testService;
	@Autowired
	TestResultService testResultService;
	
	@RequestMapping("/admin/types")
	public String controlTypes(Model model) {
		List<Type> types = typeService.getAll();
		model.addAttribute("types", types);
		List<Test> tests = testService.getAll();
		model.addAttribute("tests", tests);
		List<TestResult> results = testResultService.getAll();
		model.addAttribute("results", results);
		return "admin/allTypes";
	}

	@RequestMapping("admin/editType/{id}")
	public String editControlTypesForm(Model model, @PathVariable("id") Long id) {
		Type type = typeService.getById(id);
		model.addAttribute("type", type);
		List<Test> tests = testService.getTestsByTypeId(id);
		model.addAttribute("tests", tests);
		List<Test> testsList = new ArrayList<>();
		tests.iterator().forEachRemaining(testsList::add);
		model.addAttribute("testsListForm", new TestsList(testsList));
		List<TestResult> testResults = testResultService.getAll();
		model.addAttribute("testResults", testResults);
		List<ControlStatus> controlStatus = Arrays.asList(ControlStatus.values());
		model.addAttribute("controlStatus", controlStatus);
		List<TestStatus> testStatus = Arrays.asList(TestStatus.values());
		model.addAttribute("testStatus", testStatus);
		return "admin/editType";
	}
	 
	@RequestMapping(value = "admin/editType/{id}", method = RequestMethod.POST)
	public String editControlTypes(Model model, @Valid Type type,
			BindingResult bindingResult, Errors errors, @ModelAttribute TestsList testsListForm) {
		if (bindingResult.hasErrors()) {
			model.addAttribute("type", type);
			return "admin/editType";
		} else {
			typeService.saveType(type);
			testService.saveTests(testsListForm);
			return "redirect:/admin/types";
		}
	}
}
