package pl.bc.controllers;

import java.util.Arrays;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import pl.bc.entity.Branch;
import pl.bc.enums.BranchStatus;
import pl.bc.repository.BranchRepository;
import pl.bc.services.BranchService;

@Controller
public class BranchController {
	@Autowired
	BranchService branchService;
	@Autowired
	BranchRepository branchRepository;

	@RequestMapping("/admin/branches")
	public String allBranches(HttpServletRequest request, Model model) {
		int page = 0;
		int size = 5;
		if (request.getParameter("page") != null && !request.getParameter("page").isEmpty()) {
			page = Integer.parseInt(request.getParameter("page")) - 1;
		}
		if (request.getParameter("size") != null && !request.getParameter("size").isEmpty()) {
			size = Integer.parseInt(request.getParameter("size"));
		}
		Page<Branch>branches= branchService.getAllBranches(page, size);
		model.addAttribute("branches", branches);
		return "admin/allBranches";
	}

	@RequestMapping(value = { "/admin/addBranch" })
	public String addBranchForm(Model model) {
		model.addAttribute("branch", new Branch());
		List<BranchStatus> branchStatus = Arrays.asList(BranchStatus.values());
		model.addAttribute("branchStatus", branchStatus);
		return "admin/addBranch";
	}

	@RequestMapping(value = "/addedBranch", method = { RequestMethod.POST })
	public String addBranch(Model model,@Valid Branch branch, Errors errors) {
		if (errors.hasErrors()) {
			model.addAttribute("branch", branch);
			return "admin/addBranch";
		}
		branchService.saveBranch(branch);
		return "redirect:/admin/branches";
	}

	@RequestMapping("admin/branches/edit/{id}")
	public String editBranchForm(Model model, @PathVariable("id") int id) {
		Branch branch = branchService.findById(id);
		model.addAttribute("branch", branch);
		List<BranchStatus> branchStatus = Arrays.asList(BranchStatus.values());
		model.addAttribute("branchStatus", branchStatus);
		return "admin/editBranch";
	}

	@RequestMapping(value = "/admin/branches/branchEdit/{id}", method = RequestMethod.POST)
	public String editBranch(Model model, @Valid Branch branch, BindingResult bindingResult, Errors errors) {
		if (bindingResult.hasErrors()) {
			model.addAttribute("branch", branch);
			return "admin/editBranch";
		} else {
			branchService.updateBranch(branch);
			return "redirect:/admin/branches";
		}
	}
}
