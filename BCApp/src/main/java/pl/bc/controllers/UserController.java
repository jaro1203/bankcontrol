package pl.bc.controllers;

import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import pl.bc.entity.Branch;
import pl.bc.entity.Employee;
import pl.bc.entity.Role;
import pl.bc.entity.User;
import pl.bc.repository.BranchRepository;
import pl.bc.repository.EmployeeRepository;
import pl.bc.repository.RoleRepository;
import pl.bc.repository.UserRepository;
import pl.bc.services.UserService;

@Controller
public class UserController {
	@Autowired
	BranchRepository branchRepository;
	@Autowired
	EmployeeRepository employeeRepository;
	@Autowired
	RoleRepository roleRepository;
	@Autowired
	UserService userService;
	@Autowired
	UserRepository userRepository;
	@Autowired
	BCryptPasswordEncoder encoder;
	
	@RequestMapping(value = "/admin/users")
	public ModelAndView allUsers() {
		ModelAndView modelAndView = new ModelAndView();
		List<User> users=userService.getAllUsers();
		modelAndView.addObject("users", users);
		List<Employee> employee= employeeRepository.findAll();
		modelAndView.addObject("employee", employee);
		modelAndView.setViewName("admin/allUsers");
		return modelAndView;
	}
	
	@RequestMapping(value = "/admin/addUser")
	public ModelAndView addUserForm() {
		ModelAndView modelAndView = new ModelAndView();
		Branch branch = new Branch();
		branch = branchRepository.getOne(999);
		List<Employee> employees = employeeRepository.findByBranch(branch);
		modelAndView.addObject("employees", employees);
		List<Role> roles = roleRepository.findAll();
		modelAndView.addObject("roles", roles);
		User user = new User();
		modelAndView.addObject("user", user);
		modelAndView.setViewName("admin/addUser");
		return modelAndView;
	}

	@RequestMapping(value = "/admin/addedUser", method = RequestMethod.POST)
	public ModelAndView registerUser(@Valid User user, BindingResult bindingResult, ModelMap modelMap) {
		ModelAndView modelAndView = new ModelAndView();
		if (bindingResult.hasErrors()) {
			modelAndView.addObject("successMessage", "Proszę poprawić błędy w formularzu!");
			modelMap.addAttribute("bindingResult", bindingResult);
		} else if (userService.isUserAlreadyPresent(user)) {
			modelAndView.addObject("successMessage", "Użytkownik już istnieje!");
		}
		else {
			userService.saveUser(user);
			modelAndView.addObject("successMessage", "Użytkownik został zarejestorwany!");
		}
		modelAndView.addObject("user", new User());
		modelAndView.setViewName("admin/addUser");
		return modelAndView;
	}
	
	@RequestMapping("/admin/users/delete/{id}")
	public String deleteUser(@PathVariable("id") Long id) {
		userService.deleteUser(id);
		return "redirect:/admin/users";
	}
	
	@RequestMapping("admin/users/edit/{id}")
	public ModelAndView editUserForm(@PathVariable("id") Long id) {
		ModelAndView modelAndView = new ModelAndView();
		User user = userRepository.getOne(id);
		modelAndView.addObject("user", user);
		List<Role> roles = roleRepository.findAll();
		modelAndView.addObject("roles", roles);
		modelAndView.setViewName("admin/editUser");
		return modelAndView;
	}

	@RequestMapping(value = "/admin/users/editedUser/{id}", method = RequestMethod.POST)
	public String editUser(@Valid User user, @PathVariable("id") Long id, Model model,
			BindingResult bindingResult) {
		if (bindingResult.hasErrors()) {
			model.addAttribute("user", user);
			return "admin/editUser";
		} else {
			userService.updateUser(user);
			return "redirect:/admin/users";
		}
	}
}
