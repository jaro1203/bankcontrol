package pl.bc.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import pl.bc.entity.ControlCase;

public interface CaseRepository extends JpaRepository<ControlCase, Long> {

	@Query ("select c from ControlCase c join c.commission comm "
			+"where comm.id= :id")
	List<ControlCase> findCasesByCommission(@Param("id")Long id);

}
