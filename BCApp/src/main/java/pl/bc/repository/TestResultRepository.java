package pl.bc.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.bc.entity.TestResult;

public interface TestResultRepository extends JpaRepository<TestResult, Long> {


}
