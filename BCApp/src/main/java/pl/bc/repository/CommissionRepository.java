package pl.bc.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import pl.bc.entity.Commission;
import pl.bc.entity.Employee;

public interface CommissionRepository extends JpaRepository<Commission, Long> {

	@Query ("select c from Commission c where c.employee= :employee")
	List<Commission> findCommissionsByEpn(@Param("employee")Employee employee);

}
