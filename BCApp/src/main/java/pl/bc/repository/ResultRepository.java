package pl.bc.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import pl.bc.entity.Result;

public interface ResultRepository extends JpaRepository<Result, Long>{
	@Query ("select r from Result r "
			+ "join r.controlCase cc "
			+ "join cc.commission c "
			+ "where c.id= :id")
	List<Result>findResultByCommission(@Param("id")Long id);
}
