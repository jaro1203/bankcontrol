package pl.bc.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import pl.bc.entity.Branch;

public interface BranchRepository extends JpaRepository<Branch, Integer>{
	List<Branch> findByStatusTrue();
}
