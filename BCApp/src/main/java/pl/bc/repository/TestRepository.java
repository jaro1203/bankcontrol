package pl.bc.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import pl.bc.entity.Test;

public interface TestRepository extends JpaRepository<Test, Long> {

	@Query("select t from Test t " 
			+ "join fetch t.type ty "
			+ "join fetch ty.commission c "
			+ "where c.id= :id" )
	List<Test> getTestsByCommissionId(@Param("id") Long id);

	 @Query("select t from Test t " 
			 + "join fetch t.type ty " 
			 +"where ty.id= :id" ) 
	 List<Test> getTestsByType(@Param("id") Long id);
	 
}
