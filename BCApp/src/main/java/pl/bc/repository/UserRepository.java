package pl.bc.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.bc.entity.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {


}
