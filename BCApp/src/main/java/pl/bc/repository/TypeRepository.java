package pl.bc.repository;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import pl.bc.entity.Type;


public interface TypeRepository extends JpaRepository<Type, Long>{
	List<Type> findByStatusTrue();
}
