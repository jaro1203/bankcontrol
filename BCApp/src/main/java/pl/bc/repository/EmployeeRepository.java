package pl.bc.repository;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import pl.bc.entity.Branch;
import pl.bc.entity.Employee;

public interface EmployeeRepository extends JpaRepository<Employee, Long>{

	@Query ("select e from Employee e where e.branch= :branch")
	List<Employee> findByBranch(@Param("branch")Branch selectedBranch);

}
