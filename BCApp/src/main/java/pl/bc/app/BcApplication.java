package pl.bc.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import pl.bc.repository.TypeRepository;

@SpringBootApplication (scanBasePackages={
		"pl.bc.entity", "pl.bc.controllers", "pl.bc.repository",
		"pl.bc.services","pl.bc.security"})
@EntityScan(basePackages = { "pl.bc.entity" })
@EnableJpaRepositories (basePackageClasses = TypeRepository.class)
@EnableConfigurationProperties
public class BcApplication {

	public static void main(String[] args) {
		SpringApplication springApplication= new SpringApplication(BcApplication.class);
		//H2
		springApplication.setAdditionalProfiles("dev");	
		//MySQL
		//springApplication.setAdditionalProfiles("prod");	
		springApplication.run(args);	
	}

}
