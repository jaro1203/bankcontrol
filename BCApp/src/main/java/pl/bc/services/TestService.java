package pl.bc.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.bc.entity.Test;
import pl.bc.lists.TestsList;
import pl.bc.repository.TestRepository;

@Service
public class TestService {
	@Autowired
	private TestRepository testRepository;

	public List<Test> getAll() {
		List<Test> tests= testRepository.findAll();
		return tests;
	}

	public List<Test> getTestsByTypeId(Long id) {
		List<Test> tests= testRepository.getTestsByType(id);
		return tests;
	}

	public void saveTests(TestsList form) {
		testRepository.saveAll(form.getTests());
	}

	public List<Test> getTestsByCommissionId(Long id) {
		List<Test> tests = testRepository.getTestsByCommissionId(id);
		return tests;
	}
}
