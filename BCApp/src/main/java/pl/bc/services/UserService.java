package pl.bc.services;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import pl.bc.entity.Role;
import pl.bc.entity.User;
import pl.bc.repository.RoleRepository;
import pl.bc.repository.UserRepository;

@Service
public class UserService {
	@Autowired
	private BCryptPasswordEncoder encoder;
	@Autowired
	private RoleRepository roleRepository;
	@Autowired
	private UserRepository userRepository;

	public void saveUser(User user) {
		user.setPassword(encoder.encode(user.getPassword()));
		for (Role rola : user.getRoles()) {
			Role userRole = roleRepository.findByRole(rola.getRole()); 
			user.setRoles(new HashSet<Role>(Arrays.asList(userRole)));	
		}
		userRepository.save(user);
	}

	public boolean isUserAlreadyPresent(User user) {
		return false;
	}

	public List<User> getAllUsers() {
		List<User>users=userRepository.findAll();
		return users;
	}

	public void deleteUser(Long id) {
		User user=userRepository.getOne(id);
		userRepository.delete(user);
	}

	public void updateUser(User user) {
		User newUser= userRepository.findById(user.getEpn()).orElse(null);
		if(newUser!=null){
			newUser.setEpn(user.getEpn());
			if(!user.getPassword().equals(newUser.getPassword())){
				newUser.setPassword(encoder.encode(user.getPassword()));
			}
			newUser.setRoles(user.getRoles());
			userRepository.save(newUser);
		}
	}
}
