package pl.bc.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.bc.entity.Type;
import pl.bc.repository.TypeRepository;

@Service
public class TypeService {
	@Autowired
	private TypeRepository typeRepository;
	
	public List<Type> getAll() {
		List<Type> types= typeRepository.findAll();
		return types;
	}

	public Type getById(Long id) {
		Type type= typeRepository.findById(id).orElse(null);
		return type;
	}

	public Type getOne(long id) {
		Type type=typeRepository.getOne(id);
		return type;
	}

	public void saveType(Type type) {
		Type newType = typeRepository.getOne(type.getId());
		newType.setName(type.getName());
		newType.setStatus(type.isStatus());
		typeRepository.save(newType);
	}

	public List<Type> getActiveTypes() {
		List<Type> types= typeRepository.findByStatusTrue();
		return types;
	}

}
