package pl.bc.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pl.bc.entity.ControlCase;
import pl.bc.repository.CaseRepository;

@Service
public class ControlCaseService {
	@Autowired
	private CaseRepository caseRepository;

	public void saveCase(ControlCase controlCase) {
		caseRepository.save(controlCase);
		
	}

	public List<ControlCase> getCasesByCommission(Long id) {
		List<ControlCase> cases = caseRepository.findCasesByCommission(id);
		return cases;
	}

	public void deleteCases(List<ControlCase> deletedCases) {
		caseRepository.deleteAll(deletedCases);
	}
}
