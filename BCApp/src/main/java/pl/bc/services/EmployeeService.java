package pl.bc.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pl.bc.entity.Branch;
import pl.bc.entity.Employee;
import pl.bc.repository.EmployeeRepository;

@Service
public class EmployeeService {
	@Autowired
	private EmployeeRepository employeeRepository;
	@Autowired
	private BranchService branchService;

	public List<Employee> getByBranch(Branch branch) {
		List<Employee> employees=employeeRepository.findByBranch(branch);
		return employees;
	}

	public Employee getEmployee(Long id) {
		Employee employee=employeeRepository.getOne(id);
		return employee;
	}

	public List<Employee> getControlEmployees() {
		Branch controlDepartment = new Branch();
		controlDepartment = branchService.getBranch(999);
		List<Employee> controlEmp=employeeRepository.findByBranch(controlDepartment);
		return controlEmp;
	}

}
