package pl.bc.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pl.bc.entity.TestResult;
import pl.bc.repository.TestResultRepository;

@Service
public class TestResultService {
	@Autowired
	private TestResultRepository wynikTestuRepository;

	public List<TestResult> getAll() {
		List<TestResult> wynikiTestu=wynikTestuRepository.findAll();
		return wynikiTestu;
	}
}
