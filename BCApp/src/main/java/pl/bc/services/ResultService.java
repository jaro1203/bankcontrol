package pl.bc.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.bc.entity.Result;
import pl.bc.repository.ResultRepository;

@Service
public class ResultService {
	@Autowired
	private ResultRepository resultRepository;

	public void saveResult(Result result) {
		resultRepository.save(result);
	}

	public List<Result> getResultByCommission(Long id) {
		List<Result> result=resultRepository.findResultByCommission(id);
		return result;
	}

	public void saveAll(List<Result> results) {
		resultRepository.saveAll(results);
	}

}
