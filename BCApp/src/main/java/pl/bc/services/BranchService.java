package pl.bc.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import pl.bc.entity.Branch;
import pl.bc.repository.BranchRepository;

@Service
@Transactional
public class BranchService {
	@Autowired
	private BranchRepository branchRepository;
	
	public Page<Branch> getAllBranches(int page, int size) {
		Page<Branch> branches= branchRepository.findAll(PageRequest.of(page, size));
		return branches;
	}
	
	public Branch findById(int id) {
		Branch branch=branchRepository.findById(id).orElse(null);
		return branch;
	}
	public List<Branch> getActiveBranches() {
		List<Branch> branches=branchRepository.findByStatusTrue();
		return branches;
	}
	
	public void updateBranch(Branch branch) {
		Branch newBranch= branchRepository.findById(branch.getBrn()).orElse(null);
		if(newBranch!=null){
			newBranch.setName(branch.getName());
			newBranch.setStreet(branch.getStreet());
			newBranch.setZipCode(branch.getZipCode());
			newBranch.setCity(branch.getCity());
			newBranch.setEmail(branch.getEmail());
			newBranch.setStatus(branch.isStatus());
			branchRepository.save(newBranch);
		}
	}
	
	public void saveBranch(Branch oddzial) {
		Branch newOddzial= new Branch(oddzial.getBrn(),oddzial.getName(),oddzial.getStreet(),
				oddzial.getZipCode(),oddzial.getCity(),oddzial.getEmail(), oddzial.isStatus());
		branchRepository.save(newOddzial);	
	}

	public Branch getBranch(int i) {
		Branch branch= branchRepository.getOne(i);
		return branch;
	}


}
