package pl.bc.services;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import pl.bc.entity.Commission;
import pl.bc.entity.Employee;
import pl.bc.entity.ControlCase;
import pl.bc.entity.Test;
import pl.bc.entity.Result;
import pl.bc.lists.ResultList;
import pl.bc.repository.CommissionRepository;

@Service
public class CommissionService {
	@Autowired
	private CommissionRepository commissionRepository;
	@Autowired
	private EmployeeService employeeService;
	@Autowired
	private TestService testService;
	@Autowired
	private ControlCaseService controlCaseService;
	@Autowired
	private ResultService resultService;

	public List<Commission> getCommissions() {
		List<Commission> commissions = commissionRepository.findAll();
		return commissions;
	}
	
	public Optional <Commission> getCommissionById(Long id) {
		return commissionRepository.findById(id);
	}

	public List<Commission> getCommissionsByEpn(Employee employee) {
		return commissionRepository.findCommissionsByEpn(employee);
	}

	public Page<Commission> getAll(int page, int size) {
		Page<Commission> commissions=commissionRepository.findAll(PageRequest.of(page, size));
		return commissions;
	}
	//saveCommission
	public void saveCommission(Commission commission) {
		Commission newCommission= addNewControlCommission(commission);
		addNewControlCases(newCommission,commission);
	}
	private Commission addNewControlCommission(Commission commission) {
		Commission newCommission=new Commission(commission.getType(),
				commission.getBranch(), commission.getEmployee(),commission.getDeadline());
		return commissionRepository.save(newCommission);
	}
	private void addNewControlCases(Commission newCommission, Commission commission) {
		List<Test> tests = testService.getTestsByTypeId(commission.getType().getId());
		List<Employee> employees = employeeService.getByBranch(commission.getBranch());
		addCases(newCommission, employees, tests);
	}
	private void addCases(Commission commission,List<Employee> employees, List<Test> tests ) {
		for (Employee employee : employees) {
			ControlCase controlCase = new ControlCase(commission, employee);
			controlCaseService.saveCase(controlCase);
			addResult(controlCase, tests);
		}
	}
	private void addResult(ControlCase controlCase, List<Test> tests) {
		for (Test test : tests) {
			saveResult(controlCase, test);
		}
	}
	private void saveResult(ControlCase controlCase, Test test) {
		Result result = new Result(controlCase, test);
		resultService.saveResult(result);
	}
	
	//updateCommission
	public void updateCommission(Commission commission) {
		Commission newCommission=createCommission(commission);
		deleteOldControlCases(commission);
		addnewControlCases(commission, newCommission);
	}
	private Commission createCommission(Commission commission) {
		Commission newCommission = new Commission();
		newCommission.setId(commission.getId());
		newCommission.setType(commission.getType());
		newCommission.setBranch(commission.getBranch());
		newCommission.setEmployee(commission.getEmployee());
		newCommission.setDeadline(commission.getDeadline());
		return commissionRepository.save(newCommission);
	}
	private void deleteOldControlCases(Commission commission) {
		List<ControlCase> deletedCases = controlCaseService.getCasesByCommission(commission.getId());
		controlCaseService.deleteCases(deletedCases);
	}
	private void addnewControlCases(Commission commission, Commission newCommission) {
		List<Test> tests =testService.getTestsByTypeId(commission.getType().getId());
		List<Employee> employees= employeeService.getByBranch(commission.getBranch());
		for (Employee employee:employees) {
			createControlCase(newCommission,tests, employee);
		}
	}
	private void createControlCase(Commission newCommission, List<Test> tests, Employee employee) {
		ControlCase controlCase = new ControlCase(newCommission, employee);
		controlCaseService.saveCase(controlCase);
		addResult(controlCase, tests);
	}
	
	//deleteCommission
	public void deleteCommission(Long id) {
		commissionRepository.deleteById(id);
	}
	
	//saveControlResult
	public void saveControlResult(Long id, ResultList resultList) {
		Commission newCommission= commissionRepository.getOne(id);
		setCommissionDetails(newCommission);
		resultService.saveAll(resultList.getResults());
	}
	private void setCommissionDetails(Commission commission) {
		Commission newCommission = commission;
		newCommission.setStatus(true);
		newCommission.setDate(new Date());
		commissionRepository.save(newCommission);
	}
}
