package pl.bc.entity;

import java.util.List;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;

@Entity
public class Employee {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "employee_seq_gen")
	@SequenceGenerator(name = "employee_seq_gen", sequenceName = "EMPOLYEE_SEQ", initialValue = 10000, allocationSize = 1)
	private Long epn;
	@ManyToOne
	@JoinColumn(name = "brn")
	private Branch branch;
	private String firstName;
	private String lastName;
	private String email;
	private long positionId;
	@OneToMany(mappedBy = "employee")
	private List<ControlCase> controlCase;
	@OneToMany(mappedBy = "employee")
	private List<Commission> commission;
	@OneToOne
	private User user;

	public Employee() {

	}

	public Long getEpn() {
		return epn;
	}

	public void setEpn(Long epn) {
		this.epn = epn;
	}

	public Branch getBranch() {
		return branch;
	}

	public void setBranch(Branch branch) {
		this.branch = branch;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public long getPositionId() {
		return positionId;
	}

	public void setPositionId(long positionId) {
		this.positionId = positionId;
	}

	public List<ControlCase> getControlCase() {
		return controlCase;
	}

	public void setControlCase(List<ControlCase> controlCase) {
		this.controlCase = controlCase;
	}

	public List<Commission> getCommission() {
		return commission;
	}

	public void setCommission(List<Commission> commission) {
		this.commission = commission;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

}
