package pl.bc.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

@Entity
public class Result {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "result_seq_gen")
	@SequenceGenerator(name = "result_seq_gen", sequenceName = "RESULT_SEQ",initialValue = 200, allocationSize = 1)
	private Long id;

	@ManyToOne
	@JoinColumn(name = "test_id")
	private Test test;

	@ManyToOne
	@JoinColumn(name = "testResult_id")
	private TestResult testResult;

	@ManyToOne
	@JoinColumn(name = "controlCase_id")
	private ControlCase controlCase;

	
	public Result() {
	}
	public Result(ControlCase controlCase, Test test) {
		this.controlCase=controlCase;
		this.test= test;
	}
	public Result(ControlCase controlCase,Test test, TestResult testResult ) {
		this.controlCase=controlCase;
		this.test=test;
		this.testResult=testResult;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Test getTest() {
		return test;
	}
	public void setTest(Test test) {
		this.test = test;
	}
	public TestResult getTestResult() {
		return testResult;
	}
	public void setTestResult(TestResult testResult) {
		this.testResult = testResult;
	}
	public ControlCase getControlCase() {
		return controlCase;
	}
	public void setControlCase(ControlCase controlCase) {
		this.controlCase = controlCase;
	}
}
