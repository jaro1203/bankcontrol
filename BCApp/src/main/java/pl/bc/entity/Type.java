package pl.bc.entity;

import java.util.List;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

@Entity
public class Type {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "type_seq_gen")
	@SequenceGenerator(name = "type_seq_gen", sequenceName = "TYPE_SEQ")
	private long id;
	private String name;
	private boolean status;

	@OneToMany(mappedBy = "type")
	private List<Commission> commission;

	@OneToMany(mappedBy = "type")
	private List<Test> test;

	public Type() {

	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public List<Commission> getCommission() {
		return commission;
	}

	public void setCommission(List<Commission> commission) {
		this.commission = commission;
	}

	public List<Test> getTest() {
		return test;
	}

	public void setTest(List<Test> test) {
		this.test = test;
	}

	public static String convertStatus(boolean trueOrFalse) {
		return trueOrFalse ? "Aktywna" : "Nieaktywna";
	}

	public boolean convertStatusBack(String status) {
		if (status.equals("Aktywna")) {
			return true;
		} else {
			return false;
		}
	}

}
