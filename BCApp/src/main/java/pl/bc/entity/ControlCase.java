package pl.bc.entity;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

@Entity
public class ControlCase {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "case_seq_gen")
	@SequenceGenerator(name = "case_seq_gen", sequenceName = "CASE_SEQ",  initialValue = 100, allocationSize = 1)
	private Long id;

	@ManyToOne
	@JoinColumn(name = "commission_id")
	private Commission commission;

	@ManyToOne
	@JoinColumn(name = "epn")
	private Employee employee;

	@OneToMany(mappedBy = "controlCase", cascade= javax.persistence.CascadeType.REMOVE)
	private List<Result> controlResult;
	
	public ControlCase() {
			
	}
	public ControlCase(Commission commission, Employee employee) {
		this.commission=commission;
		this.employee=employee;
		
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Commission getCommission() {
		return commission;
	}
	public void setCommission(Commission commission) {
		this.commission = commission;
	}
	public Employee getEmployee() {
		return employee;
	}
	public void setEmployee(Employee employee) {
		this.employee = employee;
	}
	public List<Result> getControlResult() {
		return controlResult;
	}
	public void setControlResult(List<Result> controlResult) {
		this.controlResult = controlResult;
	}
	

}
