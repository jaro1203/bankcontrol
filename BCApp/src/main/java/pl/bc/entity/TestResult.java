package pl.bc.entity;

import java.util.List;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

@Entity
public class TestResult {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "testResult_seq_gen")
	@SequenceGenerator(name = "testResult_seq_gen", sequenceName = "TESTRESULT_SEQ")
	private Long id;

	@ManyToMany
	@JoinTable(name = "tests_results", joinColumns = { @JoinColumn(name = "testResult_id") }, inverseJoinColumns = {
			@JoinColumn(name = "test_id") })
	private List<Test> test;
	private String name;

	@OneToMany(mappedBy = "testResult")
	private List<Result> result;

	public TestResult() {

	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public List<Test> getTest() {
		return test;
	}

	public void settest(List<Test> test) {
		this.test = test;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Result> getResult() {
		return result;
	}

	public void setResult(List<Result> result) {
		this.result = result;
	}

}
