package pl.bc.entity;

import java.util.List;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

@Entity
public class Branch {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "branch_seq_gen")
	@SequenceGenerator(name = "branch_seq_gen", sequenceName = "BRANCH_SEQ", initialValue = 1, allocationSize = 1)
	private int brn;
	private String name;
	private String street;
	private String zipCode;
	private String city;
	private String email;
	private boolean status;

	@OneToMany(mappedBy = "branch")
	private List<Employee> employee;
	
	@OneToMany(mappedBy = "branch")
	private List<Commission> commission;
	
	public Branch() {	
	}
	

	public Branch(int brn, String name, String street, String zipCode, String city, String email,
			boolean status) {
		this.brn = brn;
		this.name = name;
		this.street = street;
		this.zipCode = zipCode;
		this.city = city;
		this.email = email;
		this.status = status;
	}
	
	public int getBrn() {
		return brn;
	}


	public void setBrn(int brn) {
		this.brn = brn;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getStreet() {
		return street;
	}


	public void setStreet(String street) {
		this.street = street;
	}


	public String getZipCode() {
		return zipCode;
	}


	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}


	public String getCity() {
		return city;
	}


	public void setCity(String city) {
		this.city = city;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public boolean isStatus() {
		return status;
	}


	public void setStatus(boolean status) {
		this.status = status;
	}


	public List<Employee> getEmployee() {
		return employee;
	}


	public void setEmployee(List<Employee> employee) {
		this.employee = employee;
	}


	public List<Commission> getCommission() {
		return commission;
	}


	public void setCommission(List<Commission> commission) {
		this.commission = commission;
	}


	public static String convertBranchStatus(boolean trueOrFalse) {
		return trueOrFalse ? "Czynny" : "Zamknięty";
	}

	public boolean convertBranchStatusBack(String status) {
		if (status.equals("Czynny")) {
			return true;
		} else {
			return false;
		}
	}

}
