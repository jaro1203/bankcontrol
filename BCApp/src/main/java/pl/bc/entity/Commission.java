package pl.bc.entity;

import java.util.Date;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

@Entity
public class Commission {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "commission_seq_gen")
	@SequenceGenerator(name = "commission_seq_gen", sequenceName = "COMMISSION_SEQ", initialValue = 100, allocationSize = 1)
	private Long id;

	@OneToMany(mappedBy= "commission",cascade= javax.persistence.CascadeType.REMOVE)
	private List<ControlCase> controlCase;

	@ManyToOne
	@JoinColumn(name = "type_id")
	private Type type;

	@ManyToOne
	@JoinColumn(name = "brn")
	private Branch branch;
	
	@ManyToOne
	@JoinColumn(name = "epn")
	private Employee employee;

	@DateTimeFormat(iso=ISO.DATE)
	private Date deadline;

	@DateTimeFormat(iso=ISO.DATE)
	@Column(nullable = true)
	private Date date;

	private boolean status;
	
	public Commission () {
	}
	
	public Commission(Type type, Branch branch,
			Employee employee, Date deadline) {
		this.type = type;
		this.branch = branch;
		this.employee = employee;
		this.deadline = deadline;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public List<ControlCase> getCase() {
		return controlCase;
	}

	public void setCase(List<ControlCase> controlCase) {
		this.controlCase = controlCase;
	}

	public Type getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
	}

	public Branch getBranch() {
		return branch;
	}

	public void setBranch(Branch branch) {
		this.branch = branch;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public Date getDeadline() {
		return deadline;
	}

	public void setDeadline(Date deadline) {
		this.deadline = deadline;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

}
