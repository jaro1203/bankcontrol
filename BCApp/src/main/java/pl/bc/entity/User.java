package pl.bc.entity;

import java.util.Set;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import org.hibernate.validator.constraints.Length;

@Entity
@Table
public class User{
	@Id
	private Long epn;
	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name="epn")
	private Employee employee;

	@NotNull(message="Hasło jest wymagane")
	@Length(min=5, message="Hasło musi składać się z minimum 5 znaków!")
	private String password;


	@ManyToMany
	@JoinTable(name = "user_role", joinColumns = @JoinColumn(name = "user_id"), inverseJoinColumns = @JoinColumn(name = "role_id"))
	private Set<Role> roles;

	public User(){
		
	}

	public User(Long epn, Set<Role> roles) {
	;
		this.epn = epn;
		this.roles = roles;
	}

	public Long getEpn() {
		return epn;
	}

	public void setEpn(Long epn) {
		this.epn = epn;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Set<Role> getRoles() {
		return roles;
	}

	public void setRoles(Set<Role> roles) {
		this.roles = roles;
	}
		

}
