package pl.bc.entity;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

@Entity
public class Test {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "test_seq_gen")
	@SequenceGenerator(name = "test_seq_gen", sequenceName = "TEST_SEQ")
	private Long id;
	private String name;
	private boolean status;

	@ManyToMany(mappedBy = "test")
	private List<TestResult> testResult;

	@ManyToOne
	@JoinColumn(name = "type_id")
	private Type type;

	@OneToMany(mappedBy = "test")
	private List<Result> result;

	public Test() {
		
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public List<TestResult> getTestResult() {
		return testResult;
	}

	public void setTestResult(List<TestResult> testResult) {
		this.testResult = testResult;
	}

	public Type getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
	}

	public List<Result> getResult() {
		return result;
	}

	public void setResult(List<Result> result) {
		this.result = result;
	}

	public static String convertTestStatus(boolean trueOrFalse) {
		return trueOrFalse ? "Aktywny" : "Nieaktywny";
	}

	public boolean convertTestStatusBack(String status) {
		if (status.equals("Aktywny")) {
			return true;
		} else {
			return false;
		}
	}
	
}
