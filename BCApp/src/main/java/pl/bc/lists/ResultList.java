package pl.bc.lists;

import java.util.ArrayList;
import java.util.List;
import pl.bc.entity.Result;

public class ResultList {

		private List <Result> results;
		
		public ResultList() {
			this.results= new ArrayList<>();
		}
		public ResultList(List<Result> results) {
			this.results= results;
		}

		public List<Result> getResults() {
			return results;
		}

		public void setResults(List<Result> results) {
			this.results = results;
		}
		
		public void addResult(Result result) {
			this.results.add(result);
		}
}
