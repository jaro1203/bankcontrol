package pl.bc.lists;

import java.util.ArrayList;
import java.util.List;

import pl.bc.entity.Test;

public class TestsList {
	
	private List<Test> tests;
	
	public TestsList() {
		this.tests= new ArrayList<>();
	}
	public TestsList(List<Test> tests) {
		this.tests= tests;
	}

	public List<Test> getTests() {
		return tests;
	}

	public void setTests(List<Test> tests) {
		this.tests = tests;
	}
	
	public void addTests(Test test) {
		this.tests.add(test);
	}
	

}
